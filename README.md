## Scopic Chat Task

This is my solution for developing a real-time chat using Laravel for the backend and React for the Front End.

### How to run

First make sure that you have these requirements :

- PHP 7.4+
- Node v12.19.0 (Stable)
- PHP Composer
- Redis server

Clone the repository and install dependencies :

```sh
$ git clone https://EslamHiko@bitbucket.org/EslamHiko/scopic-real-time-chat.git
$ composer install
$ npm install && npm run dev
$ npm install -g laravel-echo-server
```

copy the .env.example to .env and generate security key :

```sh
$ cp .env.example .env
$ php artisan key:generate
```

Set your db info in .env file and set broadcast driver to `redis` :
```sh
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=<db name>
DB_USERNAME=<db username>
DB_PASSWORD=<db password>

BROADCAST_DRIVER=redis
```
Run the migrations and seed some example data :
```sh
php artisan db:migrate
php artisan db:seed
```

Run the server & socket.io server :

```sh
php artisan serve
```
And in another tab :
```sh
laravel-echo-server start
```
