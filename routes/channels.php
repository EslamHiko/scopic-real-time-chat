<?php

use Illuminate\Support\Facades\Broadcast;
use App\Models\User;

/*
|--------------------------------------------------------------------------
| Broadcast Channels
|--------------------------------------------------------------------------
|
| Here you may register all of the event broadcasting channels that your
| application supports. The given channel authorization callbacks are
| used to check if an authenticated user can listen to the channel.
|
*/

Broadcast::channel('private.{id}', function ($user, $id) {
    $user = User::find($id);
    return $user == Auth::user() ? $user : false;
});
Broadcast::channel('public', function ($user) {
    return $user;
});
