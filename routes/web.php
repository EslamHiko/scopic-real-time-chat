<?php

use Illuminate\Support\Facades\Route;
use App\Models\User;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();
Route::middleware(['auth'])->group(function () {
  Route::get('/', function () {
      return view('chat');
  });
  Route::get('/home', function () {
      return redirect('/');
  });

  Route::get('/messages/{id?}', [App\Http\Controllers\MessagesController::class, 'getMessages']);
  Route::get('/users', function () {
    return response()->json(['users'=>User::where('id','<>',Auth::user()->id)->get()]);
  });
  Route::get('/user', function () {
    return response()->json(['user'=>Auth::user()]);
  });

  Route::post('/messages/send', [App\Http\Controllers\MessagesController::class, 'sendMessage']);
  Route::post('/messages/see/{id?}', [App\Http\Controllers\MessagesController::class, 'seeMessages']);
  Route::post('/messages/receive', [App\Http\Controllers\MessagesController::class, 'receiveMessages']);

});
