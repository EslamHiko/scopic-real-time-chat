const seeMessages = (currChatRef,
    allUsersRef,
    setAllUsers) => {
    axios.post(`/messages/see/${currChatRef.current ? currChatRef.current : 0}`).then(async (e) => {
        const newUsers = [...allUsersRef.current];
        newUsers.forEach((user, i) => {
            if (user.id == currChatRef.current) {
                newUsers[i].unread_count = 0;
                return false;
            }
        });
        allUsersRef.current = newUsers;
        await setAllUsers(newUsers);
    }).catch((error) => {
        console.log(error);
    });
}
const getListOfUsers = (allUsersRef, setAllUsers) => {
    axios.get('/users')
        .then(async (res) => {
            allUsersRef.current = res.data.users;

            await setAllUsers(res.data.users);
        })
        .catch((error) => {
            console.log(error);
        });
}


const loadMessages = async (setLoadingMessages,
    currMessagesRef,
    setCurrMessages, currChatRef,
    allUsersRef,
    setAllUsers, currPageRef, loadingMessagesRef) => {
    loadingMessagesRef.current = true;
    await setLoadingMessages(true);
    axios.get(`/messages/${currChatRef.current ? currChatRef.current : ''}`, {
            params: {
                page: currPageRef.current
            }
        })
        .then(async (res) => {
            const newMessages = res.data.messages.data ? res.data.messages.data : [];

            newMessages.reverse();
            currMessagesRef.current = currPageRef.current == 1 ? newMessages : [...newMessages, ...currMessagesRef.current];
            await setCurrMessages(currMessagesRef.current);
            loadingMessagesRef.current = false;
            setLoadingMessages(false);
            seeMessages(currChatRef,
                allUsersRef,
                setAllUsers);
            if (currPageRef.current == 1) {
                $('#msg-box').animate({
                    scrollTop: $("#msg-box")[0].scrollHeight
                })
            }
        })
        .catch((error) => {
            console.log(error);
        });
}

const sendMessage = async (body, currChatRef,
    authInfoRef,
    setMsgBody,
    currMessagesRef,
    setCurrMessages) => {
    if (!body.length)
        return;
    const temp = body;
    const tempId = new Date().getUTCMilliseconds();
    const newMessage = {
        tempId: tempId,
        body: body,
        sender: {
            ...authInfoRef.current
        }
    };
    currMessagesRef.current = [...currMessagesRef.current, newMessage];
    await setCurrMessages(currMessagesRef.current);
    $('#msg-box').animate({
        scrollTop: $("#msg-box")[0].scrollHeight
    })
    setMsgBody("");
    axios.post('/messages/send', {
            body: temp,
            id: currChatRef.current
        })
        .then(async (res) => {
            let newMessages = [...currMessagesRef.current];
            newMessages.forEach((msg, i) => {
              if(msg.tempId == tempId){
                newMessages[i] = res.data.message;
                return false;
              }
            });
            currMessagesRef.current = newMessages;
            await setCurrMessages(currMessagesRef.current);
            $('#msg-box').animate({
                scrollTop: $("#msg-box")[0].scrollHeight
            })

        })
        .catch((error) => {
            console.log(error);
        });
}

export {
    seeMessages,
    sendMessage,
    loadMessages,
    getListOfUsers,
};
