import {seeMessages} from './chat'
const subscribeToPublicChannel = (currUsersRef, setCurrUsers, allUsersRef, setAllUsers) => {
    Echo.join('public').here(async (users) => {
            const updatedUsers = {
                ...currUsersRef.current
            };
            users.forEach((user, i) => {
                updatedUsers[user.id] = true
            });
            currUsersRef.current = updatedUsers;
            await setCurrUsers(updatedUsers);

        }).joining(async (user) => {
            const updatedUsers = {
                ...currUsersRef.current
            };
            updatedUsers[user.id] = true;
            currUsersRef.current = updatedUsers;
            await setCurrUsers(updatedUsers);
            const exists = allUsersRef.current.filter(currUser => user.id == currUser.id).length;
            if (!exists) {
                updatedAllUsers = [...allUsersRef.current, user];
                allUsersRef.current = updatedAllUsers
                await setAllUsers(updatedAllUsers);
            }
        })
        .leaving(async (user) => {
            const updatedUsers = {
                ...currUsersRef.current
            };
            delete updatedUsers[user.id];
            currUsersRef.current = updatedUsers;
            await setCurrUsers(updatedUsers);
        });
}
const subscribeToPrivateChannel = (authInfoRef,
    setAuthInfo,
    currChatRef,
    currMessagesRef,
    setCurrMessages,
    allUsersRef,
    setAllUsers) => {
    axios.get('/user')
        .then(async (res) => {
            authInfoRef.current = res.data.user;
            await setAuthInfo(res.data.user);
            Echo.private(`private.${authInfoRef.current.id}`).listen('.MessageReceived', async (e) => {
                if (e.thread.id === 1 && currChatRef.current == 0 && e.message.sender.id != authInfoRef.current.id) {
                    const updatedMessages = [...currMessagesRef.current, e.message];
                    currMessagesRef.current = updatedMessages
                    await setCurrMessages(updatedMessages);
                    $('#msg-box').animate({
                        scrollTop: $("#msg-box")[0].scrollHeight
                    })
                    seeMessages(currChatRef,
                        allUsersRef,
                        setAllUsers);
                } else {
                    if (e.sender.id == currChatRef.current && e.message.sender.id != authInfoRef.current.id) {
                        const updatedMessages = [...currMessagesRef.current, e.message];
                        currMessagesRef.current = updatedMessages;
                        await setCurrMessages(updatedMessages);

                        seeMessages(currChatRef,
                            allUsersRef,
                            setAllUsers);
                        $('#msg-box').animate({
                            scrollTop: $("#msg-box")[0].scrollHeight
                        })

                    } else {
                        const newUsers = [...allUsersRef.current];
                        newUsers.forEach((user, i) => {
                            if (user.id == e.sender.id) {
                                newUsers[i].unread_count += 1;
                                return false;
                            }
                        });
                        allUsersRef.current = newUsers;
                        await setAllUsers(newUsers);

                    }
                }
            });
        })
        .catch((error) => {
            console.log(error);
        });
}

export {
    subscribeToPrivateChannel,
    subscribeToPublicChannel
}
