import React from 'react';
import ReactDOM from 'react-dom';

function Message(data) {
  const message = data.message;

    return (
      <div className="chat-message left">
          <img className="message-avatar" src="https://icon-library.com/images/default-user-icon/default-user-icon-4.jpg" alt={message.sender.name} />
          <div className="message">
              <a className="message-author" onClick={()=>{getChatWithUser(message.sender)}} href="#"> {message.sender.name}({message.sender.username}) </a>
              <span className="message-date">  {message.created_at}</span>
              <span className="message-content">{message.body}</span>
          </div>
      </div>
    );
}

export default Message;
