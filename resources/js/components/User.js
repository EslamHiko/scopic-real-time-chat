import React from 'react';
import ReactDOM from 'react-dom';

function User(data) {
  const user = data.user;
  const isOnline = data.isOnline;
  const getChatWithUser = data.getChatWithUser;
  const isActive = data.isActive;
  const currUsersRef = data.currUsersRef;
    return (
      <div style={{cursor:'pointer'}} onClick={()=>{getChatWithUser(user)}} className={`chat-user ${isActive}`}>
          <span className={`curr-status pull-right label label-primary`}>{user.username ? user.unread_count : ''}</span>
          <span className={`curr-status pull-right label label-primary ${user.username ? isOnline ? "online" : "offline" : ''}`}>{user.username ? isOnline ? "Online" : "Offline" : ''}</span>
          {user.username && <img className="chat-avatar" src="https://icon-library.com/images/default-user-icon/default-user-icon-4.jpg" alt="" />}
          <div className="chat-user-name">
              {user.name}{user.username ? `(${user.username})` : ''}
          </div>
      </div>
    );
}

export default User;
