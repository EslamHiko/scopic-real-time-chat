import React, {useEffect,useState,useRef} from 'react';
import ReactDOM from 'react-dom';
import User from '../components/User';
import Message from '../components/Message';
import { subscribeToPrivateChannel, subscribeToPublicChannel } from '../utils/channels';
import {seeMessages, sendMessage , getListOfUsers, loadMessages, currChatRef } from '../utils/chat';

function Main() {
  const [currUsers, setCurrUsers] = useState({});
  const currUsersRef = useRef(currUsers);

  const [allUsers, setAllUsers] = useState([]);
  const allUsersRef = useRef(allUsers);

  const [currPage, setCurrPage] = useState(1);
  const currPageRef = useRef(currPage);

  const [authInfo, setAuthInfo] = useState({});
  const authInfoRef = useRef(authInfo);

  const [currChat, setCurrChat] = useState(0);
  const currChatRef = useRef(currChat);

  const [currMessages, setCurrMessages] = useState([]);
  const currMessagesRef = useRef(currMessages);

  const [loadingMessages,setLoadingMessages] = useState(true);
  const loadingMessagesRef = useRef(loadingMessages);

  const [msgBody,setMsgBody] = useState("");
  const [currTitle, setCurrTitle] = useState("public");
  const [listening, setListening] = useState({});

  useEffect(() => {

    getListOfUsers(allUsersRef,setAllUsers);
    subscribeToPrivateChannel(authInfoRef,
    setAuthInfo,
    currChatRef,
    currMessagesRef,
    setCurrMessages,
    allUsersRef,
    setAllUsers);
    loadMessages(setLoadingMessages,
    currMessagesRef,
    setCurrMessages,currChatRef,
    allUsersRef,
    setAllUsers,currPageRef,loadingMessagesRef);
    subscribeToPublicChannel(currUsersRef,setCurrUsers,allUsersRef,setAllUsers);
    $('#msg-box').scroll(() => {
      if($('#msg-box').scrollTop() === 0) {
             if(!loadingMessagesRef.current){
               currPageRef.current += 1;
               setCurrPage(currPageRef);
               loadMessages(setLoadingMessages,
               currMessagesRef,
               setCurrMessages,currChatRef,
               allUsersRef,
               setAllUsers,currPageRef,loadingMessagesRef);
             }
           }
         }
       );
  },[]);

  const handleChange = (e) => {
      setMsgBody(e.target.value);
   }
   const getChatWithUser = async (user) => {
       if (currChatRef.current == user.id)
           return;
       currChatRef.current = user.id;
       await setCurrChat(user.id);
       await setCurrTitle(user.name);
       currPageRef.current = 1
       setCurrPage(currPageRef.current);
       loadMessages(
         setLoadingMessages,
         currMessagesRef,
         setCurrMessages,currChatRef,
         allUsersRef,
         setAllUsers,currPageRef,loadingMessagesRef);
   }
   const keyPress = (e) => {
      if(e.keyCode == 13){
        sendMessage(e.target.value,currChatRef,
          authInfoRef,
          setMsgBody,
          currMessagesRef,
          setCurrMessages
        )
      }
   }

    return (
      <div className="container">
        <div className="wrapper wrapper-content animated fadeInRight">
          <div className="row">
              <div className="col-lg-12">
                  <div className="ibox float-e-margins">
                      <div className="ibox-content">
                          <strong>{currTitle} Chat room</strong>
                      </div>
                  </div>
              </div>
          </div>
          <div className="row">
              <div className="col-lg-12">
                  <div className="ibox chat-view">
                      {/* <div className="ibox-title">
                          <small className="pull-right text-muted">Last message:  Mon Jan 26 2015 - 18:39:23</small> Chat room panel
                      </div> */}
                      <div className="ibox-content">
                          <div className="row">
                              <div className="col-sm-9 ">
                                  <div className="chat-discussion" id="msg-box">
                                  {loadingMessages && <div className="text-center">Loading Messages</div>}
                                  {currMessages.length > 0 && currMessages.map((msg,i)=><Message key={i} message={msg} />)}
                                  {!loadingMessages && !currMessages.length && <div className="text-center">No Messages .. Start Conversation Now!</div>}
                                  </div>

                              </div>
                              <div className="col-sm-3" >
                                  <div className="chat-users">
                                      <div className="users-list">
                                        <User user={{id:0,name:'Public Room'}} isActive={!currChat ? 'active-chat' : ''} getChatWithUser={getChatWithUser} /> {/* For accessing public chat */}
                                        {allUsers.map((user,i)=> user.unread_count > 0 && <User isActive={currChat == user.id ? 'active-chat' : ''} getChatWithUser={getChatWithUser} key={i} user={user} isOnline={currUsers[user.id]} />)}
                                        {allUsers.map((user,i)=> currUsers[user.id] && user.unread_count == 0  && <User isActive={currChat == user.id ? 'active-chat' : ''} getChatWithUser={getChatWithUser} key={i} user={user} isOnline={currUsers[user.id]} />)}
                                        {allUsers.map((user,i)=> !currUsers[user.id] && user.unread_count == 0 && <User isActive={currChat == user.id ? 'active-chat' : ''} getChatWithUser={getChatWithUser} key={i} user={user} isOnline={currUsers[user.id]} />)}
                                        <hr />

                                      </div>
                                  </div>
                              </div>
                          </div>
                          <div className="row">
                              <div className="col-lg-12">
                                  <div className="chat-message-form">
                                      <div className="form-group">
                                          <textarea className="form-control message-input" onChange={handleChange} onKeyUp={keyPress} value={msgBody} name="message" placeholder="Enter message text and press enter"></textarea>
                                      </div>
                                  </div>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
      </div>
      </div>
    );
}

export default Main;
if (document.getElementById('main')) {

    ReactDOM.render(<Main />, document.getElementById('main'));
}
