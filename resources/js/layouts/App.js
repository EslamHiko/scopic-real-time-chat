import React, {useEffect,useContext} from 'react';
import ReactDOM from 'react-dom';
import User from '../components/User';
import Message from '../components/Message';
import UsersState from '../context/UsersContext';
import UsersContext from '../context/UsersContext';
import Main from './Main';

function App() {
    return (
      <UsersState.Consumer>
        <Main />
      </UsersState.Consumer>
    );
}

export default App;
