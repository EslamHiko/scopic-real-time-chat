<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\User;
use App\Models\Participant;
use App\Models\MessageStatus;
use App\Events\MessageReceived;

class Thread extends Model
{
    use HasFactory;

    function Participants(){
      return $this->hasMany(Participant::class);
    }
    /**
     * Function to broadcast events to participating users
     * @param  Message $message 
     */
    function sendMessageToParticipants($message){
      $user_ids = $this->Participants()->pluck('user_id')->toArray();
      foreach ($user_ids as $key => $id) {
        $msgStatus = new MessageStatus();
        $msgStatus->user_id = $id;
        $msgStatus->status = "SENT";
        $msgStatus->message_id = $message->id;
        $msgStatus->save();
        event(new MessageReceived(User::find($id), $message,$this));
      }
    }
}
