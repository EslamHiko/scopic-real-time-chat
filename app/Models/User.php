<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use App\Models\Message;
use App\Models\Participant;
use App\Models\MessageStatus;

class User extends Authenticatable
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'username',
        'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    protected $appends = ['unread_count'];

    /**
     * Function to get unread messages count with the user
     * @return int Unread messages count
     */
    public function getUnreadCountAttribute()
    {
        if(!auth()->user())
          return 0;
        $curr = auth()->user();
        if ($curr->id == $this->id) {
            return 0;
        }
        $thread_id = $curr->getMutualThread($this->id);
        if (!$thread_id) {
            return 0;
        }

        $messages_ids = $curr->getThreadMessages($thread_id)->pluck('id')->toArray();
        return MessageStatus::where('user_id', $curr->id)->whereIn('message_id', $messages_ids)->where('status', '<>', 'SEEN')->count();
    }

    /**
     * Function to send message to thread
     * @param  array $data
     * @return Message
     */
    public function sendMessageToThread($data)
    {
        $thread = Thread::findOrFail($data['thread_id']);
        $message = new Message();
        $message->user_id = $this->id;
        $message->thread_id = $data['thread_id'];
        $message->body = $data['body'];
        $message->save();
        $thread->sendMessageToParticipants($message);
        $this->seeMessages([$message->id]);
        return $message;
    }
    /**
     * See all Messages in a thread
     * @param  $id Thread id
     * @return bool (True)
     */
    public function seeThread($id)
    {
        $message_ids = Message::where('thread_id', $id)->pluck('id')->toArray();

        return $this->seeMessages($message_ids);
    }
    /**
     * See Messages by array of ids
     * @param  array $message_ids
     * @return bool true
     */
    public function seeMessages($message_ids)
    {
        return MessageStatus::where('user_id', $this->id)->whereIn('message_id', $message_ids)->update(['status'=>'SEEN']);
    }
    /**
     * See messages between authenticated user and another user user
     * @param  int $user_id
     */
    public function seeUserMessages($user_id)
    {
        $thread_id = 1;

        if ($user_id) {
            $thread_id = $this->getMutualThread($user_id);
        }
        return $this->seeThread($thread_id);
    }
    /**
     * Deliver messages for further Sent->Delivered->Seen functionality
     * @return bool
     */
    public function receiveMessages()
    {
        return MessageStatus::where('user_id', $this->id)->where('status', 'SENT')->update(['status'=>'DELIVERED']);
    }
    /**
     * Function  to participate in thread
     * @param  int $id thread's id
     * @return Participant
     */
    public function participateInThread($id)
    {
        $participant = new Participant();
        $participant->user_id = $this->id;
        $participant->thread_id = $id;
        $participant->save();
        return $participant;
    }
    /**
     * Get Mutual thread between user
     * @param  int $id user's id
     * @return int    thread id
     */
    public function getMutualThread($id)
    {
        $threads = Participant::where('user_id', $id)->groupBy('thread_id')->pluck('thread_id')->toArray();
        $threads = Thread::where('is_group', 0)->whereIn('id', $threads)->where('id', '<>', 1)->pluck('id')->toArray();
        $mutual = Participant::whereIn('thread_id', $threads)->where('user_id', $this->id)->first();

        return $mutual ? $mutual->thread_id : 0;
    }
    /**
     * Function to send Message to user
     * @param  array $data
     */
    public function sendMessageToUser($data)
    {
        $mutual_thread_id = $this->getMutualThread($data['id']);
        if ($mutual_thread_id == 0) {
            $thread = $this->createThreadWithUsers([$data['id']]);
            $mutual_thread_id = $thread->id;
        }
        $data['thread_id'] = $mutual_thread_id;
        return $this->sendMessageToThread($data);
    }
    /**
     * Start thread with users for new messages
     * @param  array $ids array of users to participate fo further group chat functionality
     * @param  integer $is_group to differntial is the thread/room is group or not
     * @return Thread
     */
    public function createThreadWithUsers($ids, $is_group = 0)
    {
        $thread = new Thread();
        $thread->save();

        $participants = $this::whereIn('id', [$this->id,...$ids])->get();

        foreach ($participants as $participant) {
            $participant->participateInThread($thread->id);
        }

        return $thread;
    }
    /**
     * Function to get Messages by user id
     * @param  array $data
     */
    public function getMessages($data)
    {
        if (isset($data['id']) && $data['id']) {
            return $this->getMessagesWithUser($data['id']);
        }
        return $this->getThreadMessages(1); // public chat
    }
    /**
     * Get Thread Messages
     * @param  int $thread_id
     * @return array
     */
    public function getThreadMessages($thread_id)
    {
        /* Making sure that the user is participating in the thread
           so no user can see the chat between other users */
        if ($this->isParticipat($thread_id)) {
            return Message::where('thread_id', $thread_id)->latest()->paginate(10);
        }
        return [];
    }
    /**
     * Function to check if the user is participate In Thread or not
     * @param  int $thread_id
     * @return boolean
     */
    public function isParticipat($thread_id)
    {
        $count = Participant::where('user_id', $this->id)->where('thread_id', $thread_id)->count();
        if (!$count && $thread_id == 1) {
            $this->participateInThread($thread_id);
            return true;
        }
        return $count;
    }
    /**
     * Function to get Messages with user
     * @param  int $user_id
     * @return Message messages between the auth and the target user
     */
    public function getMessagesWithUser($user_id)
    {
        $mutual_thread_id = $this->getMutualThread($user_id);
        return $this->getThreadMessages($mutual_thread_id);
    }
    /**
     * Function to send Message
     * @param  array $data
     */
    public function sendMessage($data)
    {
        if (isset($data['id']) && $data['id']) {
            return $this->sendMessageToUser($data);
        } else {
            $data['thread_id'] = 1; // The public chat
            return $this->sendMessageToThread($data);
        }
    }
}
