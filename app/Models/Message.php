<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    use HasFactory;
    protected $with = ['sender'];
    public function sender()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
    protected $casts = [
      'created_at' => 'datetime:jS F Y h:i:s A',
    ];

    public function Thread()
    {
        return $this->belongsTo(Thread::class);
    }
}
