<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MessagesController extends Controller
{
  function getMessages(Request $request, $id = 0){
    $user = auth()->user();
    $data = $request->all();
    $data['id'] = $id;
    return response()->json(['messages'=>$user->getMessages($data)]);
  }
  function sendMessage(Request $request){
    $user = auth()->user();

    return response()->json(['message'=>$user->sendMessage($request->all())]);
  }
  function seeMessages(Request $request,$id = 0){
    return response()->json(['success'=>auth()->user()->seeUserMessages($id),'user_id' => $id]);
  }
  function receiveMessages(Request $request){
    return response()->json(['success'=>auth()->user()->receiveMessages()]);

  }
}
