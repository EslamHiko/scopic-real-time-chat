<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\Thread;
use Illuminate\Support\Facades\Hash;

class ThreadSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $thread = new Thread();
        $thread->is_group = 1;
        $thread->save();

        $name = 'Test';
        $username = 'test';
        for($i = 1; $i <= 20; $i++){
          $user = new User();
          $user->name = "Test user $i";
          $user->username = "test$i";
          $user->email = "test$i@test.com";
          $user->password = Hash::make("0123456789");
          $user->save();
          $user->participateInThread($thread->id);
          $user->sendMessage(['body'=>"Hello this is me $user->name"]);
        }
    }
}
